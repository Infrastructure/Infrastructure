Please use natural language to explain what your request is. The
only available actions are updating your email and requesting a new
GNOME Account as you have to contribute to one (or more) GNOME modules.

Please note in case you need to access multiple modules, there is no need
to request access against each of them, one request will be enough.

Examples:
  Input: "I want to update my email"
  Input: "Please give me developer access to gnome-shell"

More information can be found at:
  New accounts:
    https://handbook.gnome.org/infrastructure/developer-access.html
  E-mail updates:
    https://handbook.gnome.org/infrastructure/accounts/managing-accounts.html
