The GNOME Infrastructure Team has deprecated the former mirroring
infrastructure based on mirrorbits, therefore no further mirror
requests will be accepted. More information is available at:

https://discourse.gnome.org/t/gnome-mirroring-system-updates/25031
